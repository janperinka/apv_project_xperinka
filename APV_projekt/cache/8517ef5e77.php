<?php
// source: persons.latte

use Latte\Runtime as LR;

class Template8517ef5e77 extends Latte\Runtime\Template
{
	public $blocks = [
		'title' => 'blockTitle',
		'body' => 'blockBody',
	];

	public $blockTypes = [
		'title' => 'html',
		'body' => 'html',
	];


	function main()
	{
		extract($this->params);
?>

<?php
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('title', get_defined_vars());
?>

<?php
		$this->renderBlock('body', get_defined_vars());
?>


<?php
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['p'])) trigger_error('Variable $p overwritten in foreach on line 26');
		$this->parentName = "layout.latte";
		
	}


	function blockTitle($_args)
	{
		?>Persons<?php
	}


	function blockBody($_args)
	{
		extract($_args);
?>
    <div class="container">
    <form action="<?php
		echo $router->pathFor("index");
?>">
        <label>Napis hledany text</label>
        <input class="form-control" type="text" name="search">
        <button type="submit" class="btn btn-primary">
            <i class="fa fa-check"></i> Hledat
        </button>
    </form>
    <br>

    <table class="table table-striped">
        <tr>
            <th> Jmeno </th>
            <th> Prijmeni </th>
            <th> Prezdivka </th>
            <th> Pohlavi </th>
            <th> Vyska </th>
            <th> Datum narozeni </th>

        </tr>
<?php
		$iterations = 0;
		foreach ($persons as $p) {
?>
            <tr>
                <td>  <?php echo LR\Filters::escapeHtmlText($p['first_name']) /* line 28 */ ?> </td>
                <td>  <?php echo LR\Filters::escapeHtmlText($p['last_name']) /* line 29 */ ?>  </td>
                <td>  <?php echo LR\Filters::escapeHtmlText($p['nickname']) /* line 30 */ ?>  </td>
                <td>  <?php echo LR\Filters::escapeHtmlText($p['gender']) /* line 31 */ ?> </td>
                <td>  <?php echo LR\Filters::escapeHtmlText($p['height']) /* line 32 */ ?>  </td>
                <td>  <?php echo LR\Filters::escapeHtmlText($p['birth_day']) /* line 33 */ ?> </td>
            </tr>
<?php
			$iterations++;
		}
?>
    </table>
    </div>
<?php
	}

}
