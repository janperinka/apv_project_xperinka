<?php
// source: create-person.latte

use Latte\Runtime as LR;

class Templated350c32dbc extends Latte\Runtime\Template
{
	public $blocks = [
		'title' => 'blockTitle',
		'body' => 'blockBody',
	];

	public $blockTypes = [
		'title' => 'html',
		'body' => 'html',
	];


	function main()
	{
		extract($this->params);
?>

<?php
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('title', get_defined_vars());
?>

<?php
		$this->renderBlock('body', get_defined_vars());
?>


<?php
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		$this->parentName = "layout.latte";
		
	}


	function blockTitle($_args)
	{
		?>Create person<?php
	}


	function blockBody($_args)
	{
		extract($_args);
?>
    <div class="container">
    <form action="<?php
		echo $router->pathFor("createPerson");
?>" method="post">
        <label>Create person</label>
        First name: <input class="form-control" type="text" name="fn" required> <br>
        Last name: <input class="form-control" type="text" name="ln" required> <br>
        Nickname: <input class="form-control" type="text" name="nn" required> <br>
        Gender: <select class="form-control" name="g">
            <option value="male">Male</option>
            <option value="female">Female</option>
            <option value="">Indefined</option>
        </select> <br>
        Birthday: <input class="form-control" type="date" name="bd"> <br>
        Height: <input class="form-control" type="number" name="h"> <br>

        <button type="submit" class="btn btn-primary">
            <i class="fa fa-check"></i> Insert
        </button>
    </form>
    </div>
<?php
	}

}
